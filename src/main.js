$(document).ready(function () {
    var swiperCards = new Swiper(".product-slider", {
        observer: true,
        observeParents: true,
        pagination: {
            el: '.slider__pagination',
            bulletClass: 'slider__pagination-bullet',
            bulletActiveClass: 'slider__pagination-bullet--active',
        },
        slidesPerView: 3.05,
        spaceBetween: 20,
        containerModifierClass : 'slider',
        wrapperClass : 'slider__inner',
        slideClass : 'slider__slide',
        keyboard: {
            enabled: true,
        },
        slideToClickedSlide : true,
    });

    var wokSlider = new Swiper(".constructor__slider", {
        slidesPerView: 1,
        spaceBetween: 8,
        pagination: {
            el: '.stepper',
            clickable: true,
            renderBullet: function (index, className) {
                return '<li class="stepper__item"><span class="stepper__value">'+ (index + 1) +'</span></li>';
            },
            bulletClass: 'stepper__item',
            bulletActiveClass: 'stepper__item--active',
        },
        containerModifierClass : 'slider',
        wrapperClass : 'slider__inner',
        slideClass : 'slider__slide',
        keyboard: {
            enabled: false,
        },
        slideToClickedSlide : false,
        on: {
            slideChangeTransitionStart: function (swiper) {
                const index = swiper.realIndex;
                const totalSlides = swiper.slides.length - 1;

                $(".constructor__title-part").addClass('constructor__title-part--hided');
                $(`.constructor__title-part[data-index=${index}]`).removeClass('constructor__title-part--hided');

                if (index + 1 <= totalSlides) {
                    $("#constructor-next-btn").text('Далее');
                } else {
                    $("#constructor-next-btn").text('Готово');
                }
            },
        }
    });

    $("#constructor-next-btn").click(function () {
        const currentSlide = wokSlider.realIndex;
        const totalSlides  = wokSlider.slides.length - 1;

        if (currentSlide + 1 <= totalSlides) {
            wokSlider.slideNext();
        }
    })

    $(".constructor__product-radio").change(function () {
        $(".constructor__product-radio").closest(".product").removeClass("product--selected");
        if ($(this).is(":checked")) {
            $(this).closest(".product").addClass("product--selected");
        } else {
            $(this).closest(".product").removeClass("product--selected");
        }
    })
});

